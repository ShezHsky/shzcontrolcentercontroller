# SHZControlCenterController

[![CI Status](http://img.shields.io/travis/Thomas Sherwood/SHZControlCenterController.svg?style=flat)](https://travis-ci.org/Thomas Sherwood/SHZControlCenterController)
[![Version](https://img.shields.io/cocoapods/v/SHZControlCenterController.svg?style=flat)](http://cocoadocs.org/docsets/SHZControlCenterController)
[![License](https://img.shields.io/cocoapods/l/SHZControlCenterController.svg?style=flat)](http://cocoadocs.org/docsets/SHZControlCenterController)
[![Platform](https://img.shields.io/cocoapods/p/SHZControlCenterController.svg?style=flat)](http://cocoadocs.org/docsets/SHZControlCenterController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SHZControlCenterController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "SHZControlCenterController"

## Author

Thomas Sherwood, thomas.sherwood@degree53.com

## License

SHZControlCenterController is available under the MIT license. See the LICENSE file for more info.

