//
//  SHZControlCenterTransitioningDelegate.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import "SHZControlCenterTransitioningDelegate.h"
#import "SHZControlCenterAnimatedTransition.h"
#import "SHZControlCenterPresentationController.h"

@implementation SHZControlCenterTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
  SHZControlCenterAnimatedTransition *animator = [[SHZControlCenterAnimatedTransition alloc] init];
  return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
  SHZControlCenterAnimatedTransition *animator = [[SHZControlCenterAnimatedTransition alloc] init];
  animator.reversed = YES;
  return animator;
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source
{
  return [[SHZControlCenterPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
}

@end
