//
//  SHZControlCenterTransitionSegue.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import "SHZControlCenterTransitionSegue.h"
#import "SHZControlCenterController.h"

@interface SHZControlCenterTransitionSegue ()

@property (nonatomic, strong) SHZControlCenterController *controlCenterController;

@end

#pragma mark -

@implementation SHZControlCenterTransitionSegue

#pragma mark Overrides

- (void)perform
{
  self.controlCenterController = [[SHZControlCenterController alloc] initWithNestedController:self.destinationViewController];
  [self.sourceViewController presentViewController:self.controlCenterController animated:YES completion:nil];
}

@end
