//
//  SHZControlCenterAnimatedTransition.h
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHZControlCenterAnimatedTransition : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic, getter=isReversed) BOOL reversed;
@property (nonatomic) NSTimeInterval animationDuration;

@end
