//
//  SHZControlCenterController.h
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHZControlCenterController : UIViewController

@property (nonatomic, strong, readonly) UIViewController *nestedController;

- (instancetype)initWithNestedController:(UIViewController *)controller;
- (instancetype)initWithNestedController:(UIViewController *)controller backgroundView:(UIView *)background NS_DESIGNATED_INITIALIZER;

@end
