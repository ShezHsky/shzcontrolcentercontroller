//
//  SHZControlCenterController.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import "SHZControlCenterController.h"
#import "SHZControlCenterTransitioningDelegate.h"

@interface SHZControlCenterController ()

@property (nonatomic, strong) UIView *backingView;

@end

#pragma mark -

@implementation SHZControlCenterController

#pragma mark Designated Initializer

- (instancetype)initWithNestedController:(UIViewController *)controller
{
  UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
  UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:effect];
  return self = [self initWithNestedController:controller backgroundView:visualEffectView];
}

- (instancetype)initWithNestedController:(UIViewController *)controller backgroundView:(UIView *)background
{
  NSParameterAssert(controller);
  NSParameterAssert(background);
  
  if(self = [super init]) {
    self->_nestedController = controller;
    self->_backingView = background;
  }
  
  return self;
}


#pragma mark Overrides

- (void)loadView
{
  // We resolved the view in the initializer, now we need to install the constraints.
  self.view = self.backingView;
  
  [self.nestedController willMoveToParentViewController:self];
  [self addChildViewController:self.nestedController];
  [self.nestedController.view willMoveToSuperview:self.view];
  [self.view addSubview:self.nestedController.view];
  self.nestedController.view.translatesAutoresizingMaskIntoConstraints = NO;
  
  NSDictionary *views = @{@"view": self.nestedController.view};
  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views]];
  [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views]];
}

- (UIModalPresentationStyle)modalPresentationStyle
{
  return UIModalPresentationCustom;
}

- (id<UIViewControllerTransitioningDelegate>)transitioningDelegate
{
  return [[SHZControlCenterTransitioningDelegate alloc] init];
}

@end
