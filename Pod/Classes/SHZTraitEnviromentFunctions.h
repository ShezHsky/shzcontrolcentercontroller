//
//  SHZTraitEnviromentFunctions.h
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

BOOL SHZTraitEnviromentIsCompact(id<UITraitEnvironment> traitEnviroment);
