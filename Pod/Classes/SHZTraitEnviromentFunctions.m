//
//  SHZTraitEnviromentFunctions.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import "SHZTraitEnviromentFunctions.h"

BOOL SHZTraitEnviromentIsCompact(id<UITraitEnvironment> traitEnviroment)
{
  UITraitCollection *traits = traitEnviroment.traitCollection;
  return traits.horizontalSizeClass == UIUserInterfaceSizeClassCompact || traits.verticalSizeClass == UIUserInterfaceSizeClassCompact;
}
