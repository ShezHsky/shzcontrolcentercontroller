//
//  SHZControlCenterAnimatedTransition.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import "SHZControlCenterAnimatedTransition.h"

@implementation SHZControlCenterAnimatedTransition

#pragma mark Overrides

- (instancetype)init
{
  if(self = [super init]) {
    self->_reversed = NO;
    self->_animationDuration = 0.5;
  }
  
  return self;
}


#pragma mark UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
  return self.animationDuration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
  UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *animatingController = self.isReversed ? fromVC : toVC;
  UIView *containerView = [transitionContext containerView];
  
  CGRect presentedFrame = [transitionContext finalFrameForViewController:animatingController];
  CGRect dismissedFrame = presentedFrame;
  dismissedFrame.origin.y = CGRectGetHeight(containerView.frame);
  
  CGRect initialFrame = self.isReversed ? presentedFrame : dismissedFrame;
  CGRect finalFrame = self.isReversed ? dismissedFrame : presentedFrame;
  
  animatingController.view.frame = initialFrame;
  
  if(!self.isReversed) {
    [containerView addSubview:toVC.view];
  }
  
  __weak typeof(self) weakSelf = self;
  [UIView animateWithDuration:[self transitionDuration:transitionContext]
                        delay:0
       usingSpringWithDamping:300.0
        initialSpringVelocity:5.0
                      options:UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
    animatingController.view.frame = finalFrame;
  } completion:^(BOOL finished) {
    if(weakSelf.isReversed) {
      [fromVC.view removeFromSuperview];
    }
    
    [transitionContext completeTransition:YES];
  }];
}

@end
