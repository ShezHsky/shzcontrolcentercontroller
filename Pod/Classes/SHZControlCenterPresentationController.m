//
//  SHZControlCenterPresentationController.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 30/01/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import "SHZControlCenterPresentationController.h"
#import "SHZTraitEnviromentFunctions.h"

static const CGFloat SHZCompactControlCenterSizeFactor = 0.7;
static const CGFloat SHZRegularControlCenterSizeFactor = 0.7;

@interface SHZControlCenterPresentationController ()

@property (nonatomic, strong) UITapGestureRecognizer *chromeTapRecognizer;
@property (nonatomic, strong) UIView *chrome;

@end

#pragma mark -

@implementation SHZControlCenterPresentationController

#pragma mark Overrides

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController
{
  if(self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController]) {
    self->_chrome = [[UIView alloc] initWithFrame:CGRectZero];
    self->_chrome.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    
    self->_chromeTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chromeViewWasTapped:)];
    self->_chromeTapRecognizer.numberOfTapsRequired = 1;
    self->_chromeTapRecognizer.numberOfTouchesRequired = 1;
    [self->_chrome addGestureRecognizer:self->_chromeTapRecognizer];
  }
  
  return self;
}

- (UIModalPresentationStyle)adaptivePresentationStyle
{
  return UIModalPresentationOverCurrentContext;
}

- (void)containerViewWillLayoutSubviews
{
  self.chrome.frame = self.containerView.bounds;
  self.presentedView.frame = [self frameOfPresentedViewInContainerView];
}

- (void)presentationTransitionWillBegin
{
  self.chrome.frame = self.containerView.bounds;
  self.chrome.alpha = 0;
  [self.containerView insertSubview:self.chrome atIndex:0];
  
  __weak typeof(self) weakSelf = self;
  [[self.presentedViewController transitionCoordinator] animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    weakSelf.chrome.alpha = 1;
  } completion:nil];
}

- (void)dismissalTransitionWillBegin
{
  __weak typeof(self) weakSelf = self;
  [[self.presentedViewController transitionCoordinator] animateAlongsideTransition:^(__unused id<UIViewControllerTransitionCoordinatorContext> context) {
    weakSelf.chrome.alpha = 0;
  } completion:^(__unused id<UIViewControllerTransitionCoordinatorContext> context) {
    [weakSelf.chrome removeFromSuperview];
  }];
}

- (CGSize)sizeForChildContentContainer:(id<UIContentContainer>)container withParentContainerSize:(CGSize)parentSize
{
  // In a horizontally and vertically compact size class, we show the control center
  // like the iOS control center. Otherwise it's a modal sheet.
  if(SHZTraitEnviromentIsCompact(self)) {
    return CGSizeMake(parentSize.width, parentSize.height * SHZCompactControlCenterSizeFactor);
  }
  else {
    return CGSizeMake(parentSize.width * SHZRegularControlCenterSizeFactor, parentSize.height * SHZRegularControlCenterSizeFactor);
  }
}

- (CGRect)frameOfPresentedViewInContainerView
{
  CGRect presentedViewFrame = CGRectZero;
  UIView<UIContentContainer> *presentedView = (UIView<UIContentContainer> *)self.presentedView;
  CGSize containerViewSize = self.containerView.bounds.size;
  CGSize size = [self sizeForChildContentContainer:presentedView withParentContainerSize:containerViewSize];
  presentedViewFrame.size = size;
  
  if(SHZTraitEnviromentIsCompact(self)) {
    presentedViewFrame.origin.y = containerViewSize.height - size.height;
  }
  else {
    presentedViewFrame.origin.x = (containerViewSize.width - CGRectGetWidth(presentedViewFrame)) / 2;
    presentedViewFrame.origin.y = (containerViewSize.height - CGRectGetHeight(presentedViewFrame)) / 2;
  }
  
  return presentedViewFrame;
}


#pragma mark Target Action

- (void)chromeViewWasTapped:(UIGestureRecognizer *)recognizer
{
  if(recognizer.state == UIGestureRecognizerStateRecognized) {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
  }
}

@end
