//
//  SHZAppDelegate.h
//  SHZControlCenterController
//
//  Created by CocoaPods on 01/30/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
