//
//  SHZViewController.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 01/30/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import <SHZControlCenterController/SHZControlCenterController.h>
#import "SHZViewController.h"

@implementation SHZViewController

#pragma mark IBActions

- (IBAction)presentControlCenter:(id)sender
{
  [self performSegueWithIdentifier:@"presentControlCenter" sender:self];
}

@end
