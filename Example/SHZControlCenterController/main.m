//
//  main.m
//  SHZControlCenterController
//
//  Created by Thomas Sherwood on 01/30/2015.
//  Copyright (c) 2014 Thomas Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SHZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SHZAppDelegate class]));
    }
}
